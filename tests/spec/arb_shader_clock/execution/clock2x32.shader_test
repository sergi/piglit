[require]
GL >= 4.3
GLSL >= 4.30
GL_ARB_shader_clock

[compute shader]
#version 430
#extension GL_ARB_shader_clock: require

layout(local_size_x = 1) in;

layout(binding = 0) uniform atomic_uint good;
layout(binding = 0) uniform atomic_uint bad;

layout(std430, binding = 0) buffer ssbo_data {
	uvec2 ssbo_time[];
};

bool is_time_ordered(uvec2 a, uvec2 b) {
	int diff = int(b.y - a.y);
	if (diff > 0)
		return true;
	if (diff < 0)
		return false;

	diff = int(b.x - a.x);
	if (diff > 0)
		return true;
	return false;
}

void main() {
	uvec2 start_time = clock2x32ARB();

	ssbo_time[gl_WorkGroupID.x] = start_time;

	if (!is_time_ordered(start_time, clock2x32ARB()))
		atomicCounterIncrement(bad);
	else
		atomicCounterIncrement(good);

}

[test]
atomic counters 2
ssbo 0 64

compute 8 1 1

probe atomic counter 0 == 8
probe atomic counter 1 == 0
